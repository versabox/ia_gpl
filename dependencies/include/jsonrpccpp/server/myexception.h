#ifndef MYEXCEPTION_H
#define MYEXCEPTION_H

#include <iostream>
#include <exception>
using namespace std;

struct ExceptionPermissionDenied : public exception
{
  const char * what () const throw ()
  {
    return "Permission denied";
  }
};

struct ExceptionPermissionPartial : public exception
{
  const char * what () const throw ()
  {
    return "Partial permission";
  }
};

#endif // MYEXCEPTION_H
