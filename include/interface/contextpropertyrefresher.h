/*
    copyright (2015-2018) VersaBox sp. z o.o.
    This file is part of InterfaceApp.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CONTEXTPROPERTYREFRESHER_H
#define CONTEXTPROPERTYREFRESHER_H

#include <QQmlContext>


class ContextPropertyRefresher : public QObject
{
    Q_OBJECT

public:

    explicit ContextPropertyRefresher(QQmlContext* &context, QObject *parent = 0): context_(context), QObject(parent)
    {

    }

public slots:
    void refreshProperty(QVariant list, QString name){
        context_->setContextProperty(name, list);
    }

private:

    QQmlContext* context_;

};

#endif // CONTEXTPROPERTYREFRESHER_H
