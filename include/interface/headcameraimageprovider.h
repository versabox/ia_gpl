/*
    copyright (2015-2018) VersaBox sp. z o.o.
    This file is part of InterfaceApp.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef HEADCAMERAIMAGEPROVIDER_H
#define HEADCAMERAIMAGEPROVIDER_H

#include <QObject>
#include "QQuickImageProvider"
#include <iostream>

class HeadCameraImageProvider : public QObject, public QQuickImageProvider
{
    Q_OBJECT
public:
    HeadCameraImageProvider();
    QImage requestImage(const QString &id, QSize *size, const QSize &requestedSize);
    QImage image;

public slots:
    void newImage(QImage image);
};

#endif // HEADCAMERAIMAGEPROVIDER_H
