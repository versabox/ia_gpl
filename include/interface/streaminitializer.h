/*
    copyright (2015-2018) VersaBox sp. z o.o.
    This file is part of InterfaceApp.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef STREAMINITIALIZER_H
#define STREAMINITIALIZER_H

#include <QUdpSocket>
#include <QTimer>

#include <iostream>

class StreamInitializer : public QObject
{
    Q_OBJECT
public:
    StreamInitializer(QString ip, int port);
    ~StreamInitializer();

    void start();

private:
    QUdpSocket *udpSocket;
    QTimer timerUdpStarter;
    QString ip;
    int port;

private slots:
    void readUdpDatagram();
    void writeUdpDatagram();

signals:
    void mappedAddressReceived(QString ip, int port);
};

#endif // STREAMINITIALIZER_H
