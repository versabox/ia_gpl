/*
    copyright (2015-2018) VersaBox sp. z o.o.
    This file is part of InterfaceApp.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */
var componentButton;
var componentCheckBox;
var button= [];
var checkBox= [];

function refreshComponentList() {
    componentButton = Qt.createComponent("qrc:/qml/PushButton.qml");
    componentCheckBox = Qt.createComponent("qrc:/qml/PushButton.qml")
    if(componentButton.status === Component.Ready && componentCheckBox.status === Component.Ready){
        for(var i=0;i< button.length;i++){
            button[i].destroy();
        }
        for(var i=0;i< checkBox.length;i++){
            checkBox[i].destroy();
        }
        var difrent=client.keyPoseList.length-button.length
        keyPoseList.height +=difrent*35
        scrollMenuPanel.contentHeight +=difrent*35
        button.splice(0, button.length)
        checkBox.splice(0, checkBox.length)
        for(var j=0;j<client.keyPoseList.length;j++){
            button[j] = componentButton.createObject(keyPoseList, {"id": client.keyPoseList[j].getName,
                                                "anchors.leftMargin": 10,
                                                "width": 145,
                                                "y":(j)*35,
                                                "height": 25,
                                                "text": client.keyPoseList[j].getName,
                                                "dynamicKeyPose":true
                                                });}
        for(var j=0;j<client.keyPoseList.length;j++){
            checkBox[j] = componentCheckBox.createObject(keyPoseList, {"id": client.keyPoseList[j].getName+ "CheckBox",
                                                "width": 25,
                                                "y":(j)*35,
                                                "x": 150,
                                                "height": 25,
                                                "dynamicCheckBox": true,
                                                "clicked": client.visibleOfKeyPoses[j],
                                                "text": "",
                                                "number_of_pushbutton": j
                                                });
        }
    }
}
