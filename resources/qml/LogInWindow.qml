/*
    copyright (2015-2018) VersaBox sp. z o.o.
    This file is part of InterfaceApp.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.0
import QtQuick.Dialogs 1.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Window 2.2


Dialog {
    id:logInWindow
    visible: true
    width: 360
    height: robot.currentText === "Other" ? 240 : 260

    property var defaultPassword:"123"
    property var defaultLogin:"Peter"

    signal signalAccept(string login, string password, int robot, string ip, int port)
    signal signalReject()
    title: "HELLO VersaBox"

    function loginStatus(state){
        if(state===0)
            close()
        else if(state===1){
            loadingGif.visible = false
             Qt.createQmlObject('
                                import QtQuick 2.2
                                import QtQuick.Dialogs 1.1
                                MessageDialog {
                                    id: messageDialog;
                                    title: "Login failed";
                                    icon : StandardIcon.Warning
                                    text: "Login error"
                                    informativeText: "The password or login you’ve entered is incorrect";
                                    Component.onCompleted: visible = true;
                                }',
                                logInWindow,
                                "message")
        }
        else{
            loadingGif.visible = false
            Qt.createQmlObject('
                               import QtQuick 2.2
                               import QtQuick.Dialogs 1.1
                               MessageDialog {
                                   id: messageDialog;
                                   title: "Connection failed";
                                   icon : StandardIcon.Warning
                                   text: "Connection failed"
                                   informativeText: "No connection with the robot";
                                   Component.onCompleted: visible = true;
                               }',
                               logInWindow,
                               "message")
        }

    }

    contentItem:Rectangle{
        color:"#aaebebeb"
        Rectangle{
            color: "#00000000"
            anchors.fill: parent
            anchors.leftMargin: 20
            anchors.rightMargin: 100
            TextField{
                id: login
                width: parent.width
                anchors.top: parent.top
                anchors.topMargin: 10
                height: 30
                placeholderText: qsTr("Enter login")
            }
            TextField{
                id: password
                width: parent.width
                anchors.top:login.bottom
                anchors.topMargin: 10
                height: 30
                echoMode: TextInput.Password
                placeholderText: qsTr("Enter password")
            }
            ComboBox {
                id:robot
                width: parent.width
                anchors.top:password.bottom
                anchors.topMargin: 10
                height: 30
                model: ["Podnośnik", "Sprzęg"]
            }
            TextField{
                id: serverPort
                width: parent.width
                anchors.top: robot.bottom
                anchors.topMargin: 10
                height: 30
                text: "8383"
                inputMask: "0000"
            }
            TextField{
                id: ip
                width: parent.width
                anchors.top: serverPort.bottom
                anchors.topMargin: 10
                height: robot.currentText === "Other" ? 30 : 0
                visible: robot.currentText === "Other" ? true : false
                placeholderText: qsTr("Enter IP adress")
//                validator: RegExpValidator {regExp:  /^((?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.){0,3}(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$/}
                inputMask: "000.000.000.000;"
            }
            Row{
                id:buttons
                anchors.top: ip.bottom
                anchors.right: parent.right
                anchors.rightMargin: -20
                spacing: 20
                Button{
                    text:"Yes"
                    width: 100
                    onClicked:
                    {
                        loadingGif.visible = true
                        if(robot.currentText === "Other"){

                            signalAccept(login.text, password.text, -1, ip.text, serverPort.text)
                        }
                        else{

                            signalAccept(login.text, password.text, robot.currentIndex, ip.text, serverPort.text)
                        }
                    }

                }
                Button{
                    text:"No"
                    width: 100
                    onClicked:
                    {
                        signalReject()
                        close()
                    }
                }
            }
            Rectangle{
                anchors.top: buttons.bottom
                anchors.topMargin: 10
                border.color: "black"
                border.width: 2
                Text {
                    id: gpl
                    anchors.fill: parent
                    font.italic: true
                    font.pointSize: 7
                    color: "gray"
                    text: qsTr("
    Copyright (C) (2015-2018)  VersaBox
    This program comes with ABSOLUTELY NO WARRANTY.
    This is free software, and you are welcome to redistribute it
    under certain conditions.
                    ")
                }
            }
        }
        AnimatedImage{
            id:loadingGif
            anchors.right: parent.right
            anchors.rightMargin: 25
            y:50
            source: "qrc:/images/loading.gif"
            visible: false
        }
    }

    Component.onCompleted: {
       login.text = defaultLogin
       password.text = defaultPassword
       login.forceActiveFocus()
    }
}

