/*
    copyright (2015-2018) VersaBox sp. z o.o.
    This file is part of InterfaceApp.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.4
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import QtQuick.Controls.Styles 1.2
import "qrc:/createPushButton.js" as CreateScript

Rectangle{
    id: menuPanelContent
    property var workspaceHeight
    property int ile:0
    property var win
    property int buttonWidth: 175

    color: "gray"
    objectName: "menuPanelContent"
    width: parent.width
    height: parent.height

    function updateListOfMaps()
    {
        mapList.clear()
        for(var i=0;i<client.listOfMaps.length;i++){
            mapList.append({text: client.listOfMaps[i]})
        }
    }
    function refreshKeyPoseList() {
        CreateScript.refreshComponentList()
    }
    function addKeyPoseNow(name, description, typeOfKeyPose, typeOfTimeSet, time){

        menuPanelContent.signalAddKeyPoseAtRobotPosition(name, description, typeOfKeyPose, typeOfTimeSet, time)
        win.destroy()
    }
    function saveMapNow(name){

        menuPanel.saveMap(name)
        win.destroy()
    }
    function liftingUp(){

        if(liftUp.clicked){

            liftUp.clicked = false
        }
        else{

            liftUp.clicked = true
        }
    }

    function liftingDown(){

        if(liftDown.clicked){

            liftDown.clicked = false
        }
        else{

            liftDown.clicked = true
        }
    }

    function liftStatus(action, state){

        if(state == 0){

            if(action == 2){

                liftingUp()
                liftDown.clicked = false
            }
            else if(action == 1){

                liftingDown()
                liftUp.clicked = false
            }
        }
        else  if(state == 2){

            liftUp.clicked = true
            liftDown.clicked = false
        }
        else if(state == 1){

            liftUp.clicked = false
            liftDown.clicked = true
        }
    }

    function updateListOfKeyPosePath(){

        keyPosePathList.clear()
        for(var i = 0; i < client.keyPosePathList.length; i++){

            keyPosePathList.append({text: client.keyPosePathList[i]})
        }
    }

    function saveMidKeyPoseGraphAccept(){

        menuPanelBar.addMidKeyPoseGraph(false, true)
    }

    function saveMidKeyPoseGraphReject(){

        menuPanelBar.addMidKeyPoseGraph(false, false)
    }

    signal signalSetCameraSteeringPose(double horizontal, double vertical)
    signal signalTurnOnLaser(bool on)
    signal signalChangeMidpointsTolerance(double xy, double angle)
    signal signalLift(bool on)
    signal signalNewKeyPosePath(string name)
    signal signalStartPath(string name)
    signal signalAddKeyPoseAtRobotPosition(string name, string description, int typeOfKeyPose, int typeOfTimeSet, int time)
    signal signalLockerAction(int on)
    signal signalBrakeModeChanged(bool on)

   Flickable {
        id: scrollMenuPanel
        anchors.fill: parent
        contentHeight: layout.height + 20
        boundsBehavior: Flickable.StopAtBounds

        MouseArea{
            anchors.fill: parent
            onWheel: {

            }
        }

        ColumnLayout{
            id: layout
            spacing: 10
            anchors.top: parent.top
            anchors.topMargin: 10
            anchors.bottomMargin: 10

            Text{
                id:textMenuPanel
                text: "Control Panel"
                anchors.horizontalCenter: parent.horizontalCenter

            }
            PushButton{
                id: start
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: "Start"
                onButtonClicked:{
                    if(editPoint.clicked)
                    {
                        menuPanel.signalEditMidpoint(false)
                    }
                    addPoint.clicked=false
                    removePoint.clicked=false
                    editPoint.clicked=false
                    setRobotPosition.clicked=false
                    deleteKeyPose.clicked=false
                    addKeyPose.clicked=false
                    menuPanelBar.removeMidpoint(false)
                    menuPanelBar.addMidpoint(false)
                    menuPanelBar.startRobot()
                }
            }
            PushButton{
                id: stop
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: "Stop"
                onButtonClicked:{
                    if(editPoint.clicked)
                    {
                        menuPanel.signalEditMidpoint(false)
                    }
                    addPoint.clicked=false
                    removePoint.clicked=false
                    editPoint.clicked=false
                    deleteKeyPose.clicked=false
                    addKeyPose.clicked=false
                    setRobotPosition.clicked=false
                    menuPanelBar.removeMidpoint(false)
                    menuPanelBar.addMidpoint(false)
                    menuPanelBar.stopRobot()
                }
            }
            PushButton{
                id: setRobotPosition
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: "Set Position"
                onButtonClicked:
                    if(!setRobotPosition.clicked){
                        addPoint.clicked=false
                        removePoint.clicked=false
                        editPoint.clicked=false
                        deleteKeyPose.clicked=false
                        addKeyPose.clicked=false
                        setRobotPosition.clicked=true
                        menuPanelBar.setRobotPosition(true)
                    }else{
                        setRobotPosition.clicked=false
                        menuPanelBar.setRobotPosition(false)
                    }
            }
            Text{
                visible: false
                text: "Set type of ride"
                anchors.horizontalCenter: parent.horizontalCenter
            }
            ComboBox {
                id: rideStatus
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.right: parent.right
                model: ["To goal", "Single loop", "Infinite loop", "Random point"]
                visible: false
                onCurrentIndexChanged: {
                    menuPanelBar.changeRideMode(currentIndex)
                }
            }
            Text{
                text: "Choose Keypose Path"
                anchors.horizontalCenter: parent.horizontalCenter
            }
            ComboBox {
                id: chooseKeyPosePath
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.right: parent.right
                model: ListModel{
                    id: keyPosePathList
                }
                onCurrentIndexChanged: {

                    menuPanelContent.signalNewKeyPosePath(chooseKeyPosePath.currentText)
                }
            }
            PushButton{
                id: startPath
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: "Start Path"
                onButtonClicked:{
                    menuPanelContent.signalStartPath(chooseKeyPosePath.currentText)
                    addPoint.clicked=false
                    removePoint.clicked=false
                    editPoint.clicked=false
                    setRobotPosition.clicked=false
                    deleteKeyPose.clicked=false
                    addKeyPose.clicked=false
                    menuPanelBar.removeMidpoint(false)
                    menuPanelBar.addMidpoint(false)
                }
            }
            RowLayout{
                spacing: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                visible: false

                Text {
                    text: qsTr("vb brake mode")
                }
                Switch{
                    id:brakeMode
                    checked: true
                    onCheckedChanged: signalBrakeModeChanged(brakeMode.checked)
                }
            }
            Rectangle{
                id:test
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.rightMargin: -10
                anchors.right: parent.right
                height: 2
                color: "black"
            }
            Text{
                id:textMapPanel
                text: "Points Panel"
                anchors.horizontalCenter: parent.horizontalCenter
            }
            RowLayout{
                spacing: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                visible: false

                PushButton{
                    id: startPoint
                    width: buttonWidth - 60
                    height: 25
                    text: "Start Point"
                    onButtonClicked:{
                       menuPanelBar.changeStartingPoint(spinIndexOfStartPoint.value)
                    }

                }
                SpinBox{
                    id:spinIndexOfStartPoint
                    style: SpinBoxStyle{
                            background: Rectangle {
                                implicitWidth: 50
                                implicitHeight: 20
                                border.color: "gray"
                                radius: 10
                            }
                        }
                    minimumValue: 1
                    maximumValue: 1
                }
            }
            RowLayout{
                spacing: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                visible: false

                PushButton{
                    id: addPoint
                    width: buttonWidth - 60
                    height: 25
                    text: "Add point"
                    onButtonClicked:{
                        if(!addPoint.clicked){
                            removePoint.clicked=false
                            addPoint.clicked=true
                            editPoint.clicked=false
                            setRobotPosition.clicked=false
                            deleteKeyPose.clicked=false
                            addKeyPose.clicked=false
                            menuPanelBar.addMidpoint(true)
                        }else{
                            addPoint.clicked=false
                            menuPanelBar.addMidpoint(false)
                        }
                    }

                }
                SpinBox{
                    id:spinIndexOfAddPoint
                    style: SpinBoxStyle{
                            background: Rectangle {
                                implicitWidth: 50
                                implicitHeight: 20
                                border.color: "gray"
                                radius: 10
                            }
                        }
                    minimumValue: 1
                    maximumValue: 1
                    onValueChanged: menuPanel.indexOfAddingMidpoint=spinIndexOfAddPoint.value-1
                }
            }
            PushButton{
                id: editPoint
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: "Edit point"
                visible: false
                onButtonClicked:
                    if(!editPoint.clicked){
                        addPoint.clicked=false
                        removePoint.clicked=false
                        editPoint.clicked=true
                        setRobotPosition.clicked=false
                        deleteKeyPose.clicked=false
                        addKeyPose.clicked=false
                        menuPanelBar.editMidpoint(true)
                    }else{
                        editPoint.clicked=false
                        menuPanelBar.editMidpoint(false)
                    }
            }
            PushButton{
                id: removePoint
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: "Remove point"
                visible: false
                onButtonClicked:
                    if(!removePoint.clicked){
                        addPoint.clicked=false
                        removePoint.clicked=true
                        editPoint.clicked=false
                        setRobotPosition.clicked=false
                        deleteKeyPose.clicked=false
                        addKeyPose.clicked=false
                        menuPanelBar.removeMidpoint(true)
                    }else{
                        removePoint.clicked=false
                        menuPanelBar.removeMidpoint(false)
                    }
            }
            PushButton{
                id: removeAllMidpoints
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: "Remove all points"
                onButtonClicked:{
                    addPoint.clicked=false
                    removePoint.clicked=false
                    editPoint.clicked=false
                    setRobotPosition.clicked=false
                    menuPanelBar.removeAllMidpoint()
                }
            }
            PushButton{
                id: addMidKeyPoseGraph
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 50
                text: "          Add \nMidKeyPoseGraph"
                onButtonClicked:{
                    if(!addMidKeyPoseGraph.clicked){

                        addPoint.clicked=false
                        removePoint.clicked=false
                        editPoint.clicked=false
                        setRobotPosition.clicked=false
                        addMidKeyPoseGraph.clicked = true
                        addMidKeyPoseGraph.text = "          Save \nMidKeyPoseGraph"
                        menuPanelBar.addMidKeyPoseGraph(true, false)
                    }
                    else{

                        addMidKeyPoseGraph.clicked = false
                        addMidKeyPoseGraph.text =  "          Add \nMidKeyPoseGraph"
                        var component = Qt.createComponent("qrc:/qml/SaveDialog.qml")
                        win = component.createObject(menuPanelContent, {
                                                "title": "MidKeyPoseGraph Saver",
                                                "nameText": false,
                                                "descriptionText": false,
                                                "warningText": true,
                                                "warningString": "Do you want to save changes ?",
                                                "connectAcceptFunction": menuPanelContent.saveMidKeyPoseGraphAccept,
                                                "connectRejectFunction": menuPanelContent.saveMidKeyPoseGraphReject
                                                });
                    }
                }
            }
            RowLayout{
                spacing: 37
                anchors.left: parent.left
                anchors.leftMargin: 10
                Text {
                    id: visibleOfMidKeyPoseGraph
                    text: qsTr("Set Visible\n of graph")
                }
                PushButton{
                    id:visibleOfMidKeyPoseGraphPB
                    clicked: false
                    width: 25
                    height: 25
                    dynamicCheckBox: true
                    text:""
                    x: 130
                    onButtonClicked:{
                        if(!visibleOfMidKeyPoseGraphPB.clicked){

                            menuPanelBar.visibleOfMidKeyPoseGraph(true)
                        }
                        else{

                            menuPanelBar.visibleOfMidKeyPoseGraph(false)
                        }
                    }
                }
            }

            Rectangle{
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.rightMargin: -10
                anchors.right: parent.right
                height: 2
                color: "black"
            }

            Text{
                anchors.left: parent.left
                anchors.leftMargin: 20
                width: parent.width
                text: "Midpoints\ntolerance:"
                visible: false
            }

            RowLayout{
                spacing: 20
                anchors.left: parent.left
                anchors.leftMargin: 20
                visible: false
                Rectangle{
                    width: 40
                    color:"#00000000"
                    height:20
                    Text{
                        width:20
                        height: 20
                        text: "XY:"
                    }
                }
                SpinBox{
                    id: toleranceXY
                    decimals: 2
                    minimumValue: 0
                    stepSize: 0.5
                }
            }

            RowLayout{
                spacing: 20
                anchors.left: parent.left
                anchors.leftMargin: 20
                visible: false
                Rectangle{
                    width: 40
                    color:"#00000000"
                    height:20
                    Text{
                        width:20
                        height: 20
                        text: "Angle:"
                    }
                }
                SpinBox{
                    id: toleranceAngle
                    decimals: 2
                    minimumValue: 0
                    stepSize: 0.5
                }
            }

            PushButton{
                id: midpointsTolerance
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: "Set tolerance"
                visible: false
                onButtonClicked:{
                    signalChangeMidpointsTolerance(toleranceXY.value,toleranceAngle.value)
                }
            }


            Rectangle{
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.rightMargin: -10
                anchors.right: parent.right
                height: 0
                color: "black"
            }
            Text{
                id:textArea2
                text: "Map Panel"
                anchors.horizontalCenter: parent.horizontalCenter
            }
            ComboBox {
                id: mapChange
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.right: parent.right
                model: ListModel{
                    id: mapList
                }
                onPressedChanged: menuPanelBar.updateListOfMaps()
            }

            RowLayout{
                spacing: 37
                anchors.left: parent.left
                anchors.leftMargin: 10
                Text {
                    id: visualMap
                    text: qsTr("Visual type\n of map")
                }
                PushButton{
                    id: visual
                    clicked: false
                    width: 25
                    height: 25
                    dynamicCheckBox: true
                    text:""
                    x: 130
                }
            }

            PushButton{
                id: loadMap
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: "Load map"
                onButtonClicked:{
                    removePoint.clicked=false
                    addPoint.clicked=false
                    editPoint.clicked=false
                    setRobotPosition.clicked=false
                    deleteKeyPose.clicked=false
                    addKeyPose.clicked=false
                    menuPanelBar.loadMap(mapChange.currentIndex, visual.clicked)
                }
            }
            PushButton{
                id: bulidMap
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: "Bulid new map"
                onButtonClicked:{
                    removePoint.clicked=false
                    addPoint.clicked=false
                    editPoint.clicked=false
                    setRobotPosition.clicked=false
                    deleteKeyPose.clicked=false
                    addKeyPose.clicked=false
                    menuPanelBar.bulidNewMap()
                }
            }
            PushButton{
                id: saveMap
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: "Save map"
                onButtonClicked:{
                    addPoint.clicked=false
                    removePoint.clicked=false
                    editPoint.clicked=false
                    setRobotPosition.clicked=false
                    var component = Qt.createComponent("qrc:/qml/SaveDialog.qml")
                    win = component.createObject(menuPanelContent, {
                                            "title": "Keypose Saver",
                                            "nameText": true,
                                            "descriptionText": false,
                                            "connectAcceptFunction": menuPanelContent.saveMapNow,
                                            });
                }
            }

            Rectangle{
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.rightMargin: -10
                anchors.right: parent.right
                height: 2
                color: "black"
            }
            Text{
                id:textArea1
                text: "Keypose Panel"
                anchors.horizontalCenter: parent.horizontalCenter
            }
            PushButton{
                id: addKeyPose
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: "Add KeyPose"
                onButtonClicked:{
                    if(!addKeyPose.clicked){
                        removePoint.clicked=false
                        addPoint.clicked=false
                        editPoint.clicked=false
                        setRobotPosition.clicked=false
                        deleteKeyPose.clicked=false
                        editKeyPose.clicked=false
                        addKeyPose.clicked=true
                        menuPanelBar.addKeyPose(true)
                    }else{
                        addKeyPose.clicked=false
                        menuPanelBar.addKeyPose(false)
                    }
                }
            }
            PushButton{
                id: addKeyPoseAtRobot
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 50
                text: "Add KeyPose\n   at Robot"
                onButtonClicked:{
                    var component = Qt.createComponent("qrc:/qml/KeyPoseSaveDialog.qml")
                    win = component.createObject(menuPanelContent, {
                                            "nameText": true,
                                            "descriptionText": true,
                                            "connectAcceptFunction": menuPanelContent.addKeyPoseNow,
                                            });
                    removePoint.clicked=false
                    addPoint.clicked=false
                    editPoint.clicked=false
                    setRobotPosition.clicked=false
                    addKeyPose.clicked=false
                    deleteKeyPose.clicked=false
                    editKeyPose.clicked=false
                }
            }
            PushButton{
                id: editKeyPose
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: "Edit KeyPose"
                onButtonClicked:{
                    if(!editKeyPose.clicked){
                        removePoint.clicked=false
                        addPoint.clicked=false
                        editPoint.clicked=false
                        setRobotPosition.clicked=false
                        deleteKeyPose.clicked=false
                        addKeyPose.clicked=false
                        editKeyPose.clicked=true
                        menuPanelBar.editKeyPose(true)
                    }else{
                        editKeyPose.clicked=false
                        menuPanelBar.editKeyPose(false)
                    }
                }
            }
            PushButton{
                id: deleteKeyPose
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: "Delete KeyPose"
                onButtonClicked:{
                    if(!deleteKeyPose.clicked){
                        removePoint.clicked=false
                        addPoint.clicked=false
                        editPoint.clicked=false
                        setRobotPosition.clicked=false
                        addKeyPose.clicked=false
                        deleteKeyPose.clicked=true
                        menuPanelBar.deleteKeyPose(true)
                    }else{
                        deleteKeyPose.clicked=false
                        menuPanelBar.deleteKeyPose(false)
                    }
                }
            }
            PushButton{
                id: dockApproachFront
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: "Dock Approach Front"
                onButtonClicked:{
                    menuPanelBar.dockApproach(1);
                }
            }
            PushButton{
                id: dockApproachBack
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: "Dock Approach Back"
                onButtonClicked:{
                    menuPanelBar.dockApproach(-1);
                }
            }
            PushButton{
                id: dockDepartureFront
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: "Dock Departure Front"
                onButtonClicked:{
                    menuPanelBar.dockDeparture(1);
                }
            }
            PushButton{
                id: dockDepartureBack
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: "Dock Departure Back"
                onButtonClicked:{
                    menuPanelBar.dockDeparture(-1);
                }
            }
            PushButton{
                id: sideButton
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: "Finish Procedure"
                onButtonPressed: {
                    menuPanelBar.sideButtonClicked(true);
                }

                onButtonReleased: {
                    menuPanelBar.sideButtonClicked(false);
                }
            }
            PushButton{
                id: stopCharging
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: "Stop Charging"
                onButtonClicked:{
                    menuPanelBar.signalStopCharingButtonClicked();
                }
            }
            PushButton{
                id: skipButton
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: "Skip Keypose"
                onButtonClicked:{
                    menuPanelBar.skipButtonClicked();
                }
            }
            RowLayout{
                spacing: 37
                anchors.left: parent.left
                anchors.leftMargin: 10
                Text {
                    id: visibleOfAllKeyposeText
                    text: qsTr("Set Visible\n of Keypose")
                }
                PushButton{
                    id:visibleOfAllKeypose
                    clicked: true
                    width: 25
                    height: 25
                    dynamicCheckBox: true
                    text:""
                    number_of_pushbutton: -1
                    x: 130
                }
            }
            Rectangle{
                id:keyPoseList
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.rightMargin: 10
                anchors.right: parent.right
                height: 1
                color: "#00000000"
                //border.color: "black"
//                Flickable{
//                    id:keyPoseList
//                    anchors.fill: parent
//                    contentHeight: keyPoseRect.height
//                    boundsBehavior: Flickable.StopAtBounds

//                    clip:true
//                    Rectangle{
//                        id:keyPoseRect
//                        anchors.left: parent.left
//                        anchors.leftMargin: 10
//                        anchors.rightMargin: -10
//                        anchors.right: parent.right
//                        height: 0
//                        color: "#00000000"
//                    }
//                }

            }
            Rectangle{
                id:cameraPanel
                anchors.top:keyPoseList.bottom
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.rightMargin: -10
                anchors.right: parent.right
                height: 2
                color: "black"
                visible: false
            }
            PushButton{
                id: cameraSteeringPose
                anchors.top: cameraPanel.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 0
                text: "     Camera\n Steering Pose"
                visible: false
                onButtonClicked:{
                        removePoint.clicked=false
                        addPoint.clicked=false
                        editPoint.clicked=false
                        setRobotPosition.clicked=false
                        addKeyPose.clicked=false
                        deleteKeyPose.clicked=false
                        menuPanelContent.signalSetCameraSteeringPose(0,-1.09)
                }
            }
            PushButton{
                id: turnOnLaser
                anchors.top: cameraSteeringPose.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 0
                text: "Turn off \n Laser"
                clicked: true
                visible: false
                onButtonClicked:{
                    if(turnOnLaser.clicked){

                        turnOnLaser.clicked = false
                        menuPanelContent.signalTurnOnLaser(false)
                        turnOnLaser.text = "Turn on \n Laser"
                    }
                    else{

                        menuPanelContent.signalTurnOnLaser(true)
                        turnOnLaser.clicked = true
                        turnOnLaser.text = "Turn off \n Laser"
                    }

                }
            }
            PushButton{
                id: liftUp
                anchors.top: cameraPanel.bottom //turnOnLaser.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: "Lift Up"
                onButtonClicked:{
                    menuPanelContent.signalLift(true)
                }
            }

            PushButton{
                id: liftDown
                anchors.top: liftUp.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: "Lift Down"
                onButtonClicked:{
                    menuPanelContent.signalLift(false)
                }
            }
            PushButton{
                id: lock
                anchors.top: liftDown.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: "Lock"
                onButtonClicked:{
                    menuPanelContent.signalLockerAction(0)
                }
            }

            PushButton{
                id: unlock
                anchors.top: lock.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: "Unlock"
                onButtonClicked:{
                    menuPanelContent.signalLockerAction(1)
                }
            }
//            PushButton{
//                id: turnOffLaser
//                anchors.top: turnOnLaser.bottom
//                anchors.topMargin: 10
//                anchors.left: parent.left
//                anchors.leftMargin: 10
//                width: 125
//                height: 50
//                text: "Turn off \n Laser"
//                onButtonClicked:{
//                menuPanelContent.signalTurnOnLaser(false)
//                }
//            }

            PushButton{
                id: addWarningZone
                anchors.top: unlock.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 50
                text: " Add \nWarningZone"
                onButtonClicked:{
                    if(!addWarningZone.clicked){
                        addPoint.clicked=false
                        removePoint.clicked=false
                        editPoint.clicked=false
                        setRobotPosition.clicked=false
                        addWarningZone.clicked = true
                        var component = Qt.createComponent("qrc:/qml/SaveDialog.qml")
                        win = component.createObject(menuPanelContent, {
                                                "title": "Choose warning zone type",
                                                "nameText": false,
                                                "descriptionText": false,
                                                "typeVisible": true,
                                                "connectAcceptFunction": menuPanel.addWarningZone,
                                                "typeModel": ["POLYGON", "CIRCLE"],
                                                });
                    }
                    else{

                        addWarningZone.clicked = false
                        menuPanel.stopEditing()
                    }
                }
            }
            PushButton{
                id: deleteWarningZone
                anchors.top: addWarningZone.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 50
                text: " Delete \nWarningZone"
                onButtonClicked:{
                    if(!deleteWarningZone.clicked){
                        addPoint.clicked=false
                        removePoint.clicked=false
                        editPoint.clicked=false
                        setRobotPosition.clicked=false
                        addWarningZone.clicked = false
                        deleteWarningZone.clicked = true
                        menuPanel.deleteWarningZone()
                    }
                    else{

                        deleteWarningZone.clicked = false
                        menuPanel.stopEditing()
                    }
                }
            }
            RowLayout{
                id:visibleOfWarningZone
                spacing: 17
                anchors.top: deleteWarningZone.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                Text {
                    id: visibleOfWarningZoneText
                    text: qsTr("Set Visible\n of WarningZone")
                }
                PushButton{
                    id:visibleOfWarningZonePB
                    clicked: false
                    width: 25
                    height: 25
                    dynamicCheckBox: true
                    text:""
                    number_of_pushbutton: -50
                    x: 130
                    onButtonClicked:{
                        if(!visibleOfWarningZonePB.clicked){

                            menuPanelBar.visibleOfWarningZone(true)
                        }
                        else{

                            menuPanelBar.visibleOfWarningZone(false)
                        }
                    }
                }
            }
            PushButton{
                id: addNoDrivingArea
                anchors.top: visibleOfWarningZone.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 50
                visible: false
                text: "         Add \nNoDrivingArea"
                onButtonClicked:{
                    if(!addNoDrivingArea.clicked){

                        addPoint.clicked=false
                        removePoint.clicked=false
                        editPoint.clicked=false
                        setRobotPosition.clicked=false
                        addNoDrivingArea.clicked = true
                        addNoDrivingArea.text = "         Save \nNoDrivingArea"
                        menuPanelBar.addNoDrivingArea(true)
                    }
                    else{

                        addNoDrivingArea.clicked = false
                        addNoDrivingArea.text =  "         Add \nNoDrivingArea"
                        menuPanelBar.addNoDrivingArea(false)
//                        var component = Qt.createComponent("qrc:/qml/SaveDialog.qml")
//                        win = component.createObject(menuPanelContent, {
//                                                "title": "MidKeyPoseGraph Saver",
//                                                "nameText": false,
//                                                "descriptionText": false,
//                                                "warningText": true,
//                                                "warningString": "Do you want to save changes ?",
//                                                "connectAcceptFunction": menuPanelContent.saveMidKeyPoseGraphAccept,
//                                                "connectRejectFunction": menuPanelContent.saveMidKeyPoseGraphReject
//                                                });
                    }
                }
            }

            Rectangle{
                id:last
                anchors.top: addNoDrivingArea.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.rightMargin: -10
                anchors.right: parent.right
                height: 2
                color: "black"
            }
        }

        states: [
            State {
                name:"shownMenu"
                PropertyChanges {target: scrollMenuPanel; contentY:textMenuPanel.y}
            },
            State {
                name:"shownMap"
                PropertyChanges { target: scrollMenuPanel; contentY:textMapPanel.y}
            },
            State {
                name:"shownArea2"
                PropertyChanges {target: scrollMenuPanel; contentY:textArea2.y}
            },
            State {
                name:"shownArea1"
                PropertyChanges { target: scrollMenuPanel; contentY:textArea1.y}
            },
            State {
                name:"top"
                PropertyChanges { target: scrollMenuPanel; contentY:0}
            },
            State {
                name:"bottom"
                PropertyChanges { target: scrollMenuPanel; contentY:scrollMenuPanel.contentHeight-scrollMenuPanel.height}
            }
        ]

        transitions: [
            Transition {
                    NumberAnimation { target:scrollMenuPanel; properties: "contentY" ; easing.type: Easing.OutExpo; duration: 1500 }
            }
        ]

        Connections{
            target: menuPanelBar
            onSignalShownMenu:{
                if(layout.height<=scrollMenuPanel.height)
                {
                    scrollMenuPanel.state="top"

                }
                else if(layout.height-textMenuPanel.y<=scrollMenuPanel.height)
                {
                    scrollMenuPanel.state="bottom"
                }
                else
                {
                    scrollMenuPanel.state="shownMenu"
                }
            }
            onSignalShownMap:{
                if(layout.height<=scrollMenuPanel.height)
                {
                    scrollMenuPanel.state="top"

                }
                else if(layout.height-textMapPanel.y<=scrollMenuPanel.height)
                {
                    scrollMenuPanel.state="bottom"
                }
                else
                {
                    scrollMenuPanel.state="shownMap"
                }
            }
            onSignalShownArea2:{
                if(layout.height<=scrollMenuPanel.height)
                {
                    scrollMenuPanel.state="top"

                }
                else if(layout.height-textArea2.y<=scrollMenuPanel.height)
                {
                    scrollMenuPanel.state="bottom"
                }
                else
                {
                    scrollMenuPanel.state="shownArea2"
                }
            }
            onSignalShownArea1:{
                if(layout.height<=scrollMenuPanel.height)
                {
                    scrollMenuPanel.state="top"

                }
                else if(layout.height-textArea1.y<=scrollMenuPanel.height)
                {
                    scrollMenuPanel.state="bottom"
                }
                else
                {
                    scrollMenuPanel.state="shownArea1"
                }
            }
            onSignalChangeValueOfSpinBox:{
                    spinIndexOfAddPoint.maximumValue=client.midpoints.length/3+1
                    spinIndexOfAddPoint.value=client.midpoints.length/3+1
                    spinIndexOfStartPoint.maximumValue=client.midpoints.length/3+1
            }
        }
    }
}
