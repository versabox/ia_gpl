/*
    copyright (2015-2018) VersaBox sp. z o.o.
    This file is part of InterfaceApp.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.0
import QtQuick.Dialogs 1.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

Dialog {
    width: 150
//    height: descriptionText ? 150 : 70
    visible: true
    title: "" //set it when create widnow

    property bool nameText: true
    property bool descriptionText: false
    property bool typeVisible: false
    property bool warningText: false
    property string nameShadowText: qsTr("Enter name")
    property string nameShadowDescription: qsTr("Enter descritption")
    property string warningString: qsTr("")
    property string oldNameText: ""
    property string oldDescriptionText: ""
    property var connectAcceptFunction
    property var connectRejectFunction
    property var typeModel: []

    standardButtons: StandardButton.Cancel | StandardButton.Ok

    signal signalAccept(string name, string description, int type)
    signal signalReject()

    Text {
        id: warning
        anchors.margins: 10
        visible: warningText
        width: parent.width
        height: warningText ? 30 : 0
        text: warningString
    }

    TextField{
        id: name
        anchors.margins: 10
        visible: nameText
        width: parent.width
        height: nameText ? 30 : 0
        focus: true
        placeholderText: nameShadowText
    }

    TextField{
        id: description
        anchors.top: name.bottom
        anchors.margins: 10
        visible: descriptionText
        width: parent.width
        height: descriptionText ? 60 : 0
        placeholderText: nameShadowDescription
    }

    ComboBox {
        id: type
        anchors.top: description.bottom
        anchors.margins: 10
        visible: typeVisible
        width: parent.width
        height: typeVisible ? 30 : 0
        model: typeModel
    }

    onAccepted: {

        signalAccept(name.text, description.text, type.currentIndex)
    }
    onRejected: {

        signalReject()
    }

    Component.onCompleted: {
        signalAccept.connect(connectAcceptFunction)
        if(connectRejectFunction){

            signalReject.connect(connectRejectFunction)
        }
        if(oldNameText !== ""){

            name.text = oldNameText
        }
        if(oldDescriptionText !==""){

            description.text = oldDescriptionText
        }
        name.forceActiveFocus()
    }
}
