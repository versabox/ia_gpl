/*
    copyright (2015-2018) VersaBox sp. z o.o.
    This file is part of InterfaceApp.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.0

Rectangle {
    id: statusItem
    height: 30
    width: 30
    color: "#ffebebeb"
    property alias animationItem: animationIcon.source
    property alias textItem: informationText.text
    property alias textHeight: decription.height
    property alias textWidth: decription.width
    property alias colorBox: statusItem.color

    signal descriptionShown()

    function hideDescription() {
        hideDelayer.running = false
    }

    onStateChanged: {
            descriptionShown()
    }
    AnimatedImage{
        id:animationIcon
        anchors.fill:statusItem

        MouseArea {
            id: hoverDetector
            anchors.fill: animationIcon
            hoverEnabled: true

            onExited: hideDelayer.running = true
        }

        Item {
            id: decription
            x: 30
            y: 5
            opacity: 0

            Rectangle{
                width: parent.width
                height: parent.height
                color: "#aaebebeb"
                Text {
                    id: informationText
                }
            }

            MouseArea {
                id: hoverDetectorInDescription
                anchors.fill: decription
                hoverEnabled: parent.opacity !== 0
                propagateComposedEvents: true

                onExited: hideDelayer.running = true
            }
        }

        Timer {
            id: hideDelayer
            interval: 1000
            running: false
            repeat: false
        }
    }

    states: [
        State {
            name: "mouseOver"
            when: {
//                loader.sourceComponent && ( hoverDetector.containsMouse || hoverDetectorInDescription.containsMouse || hideDelayer.running )
                hoverDetector.containsMouse || hoverDetectorInDescription.containsMouse || hideDelayer.running
            }

            PropertyChanges { target: decription; opacity: 1}
        }
    ]

    transitions: [
        Transition {
            PropertyAnimation { target: decription; properties: "opacity"; easing.type: Easing.OutQuint; duration: 500}
        }
    ]
}

