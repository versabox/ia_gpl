/*
    copyright (2015-2018) VersaBox sp. z o.o.
    This file is part of InterfaceApp.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.0

Rectangle {
    id:statusPanelContent
    width: box.width + 20
    height: box.height + 20
    color: "#a1a1a1"
    objectName: "statusPanelContent"

    function batteryState(low, critical){
        var power=client.batteryPower

        battery.color = "#FFEBEBEB"
        if(low) {
            battery.color = "#FFFF00"
        }
        if(critical) {
            battery.color = "#FF0000"
        }

        if(power >=0){
            if(power>85){
                batteryRect.state = "battery4"
            }
            else if(power>65){
                batteryRect.state = "battery3"
            }
            else if(power>45){
                batteryRect.state = "battery2"
            }
            else if(power>25){
                batteryRect.state = "battery1"
            }
            else{
                batteryRect.state = "battery0"
            }

            battery.textItem="  " + power + "  %"
        }
        else if(power === -1){
            batteryRect.state = "charging"
            battery.textItem="charging"
        }
    }

    Column {
        id: box
        spacing: 10
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        function hideAllExcept(exceptId)
        {
            for (var i = 0; i < box.children.length; ++i)
                if (box.children[i].self !== exceptId)
                    box.children[i].hideDescription()
        }

        StatusItem{
            id:wifi
            property string self: "wifi"
            onDescriptionShown: box.hideAllExcept(self)
            animationItem: "qrc:/images/wifi.gif"
            Rectangle{
                id:wifiRect
                state: "change"
                states: [
                    State {
                        name:"change"
                        PropertyChanges {target:wifi; animationItem:"qrc:/images/wifi.gif" }
                    },
                    State {
                        name:"wifi0"
                        PropertyChanges {target:wifi; animationItem:"qrc:/images/wifi0.gif" }
                    },
                    State {
                        name:"wifi1"
                        PropertyChanges {target:wifi; animationItem:"qrc:/images/wifi1.gif" }
                    },
                    State {
                        name:"wifi2"
                        PropertyChanges {target:wifi; animationItem:"qrc:/images/wifi2.gif" }
                    },
                    State {
                        name:"wifi3"
                        PropertyChanges {target:wifi; animationItem:"qrc:/images/wifi3.gif" }
                    }
                ]
            }
        }

        StatusItem {
            property string self: "costam"
            onDescriptionShown:box.hideAllExcept(self)
        }
        StatusItem {
            id:battery
            property string self: "battery"
            onDescriptionShown:box.hideAllExcept(self)
            textHeight: 20
            textWidth: 70
            animationItem: "qrc:/images/battery.gif"
            colorBox: "#ffebebeb"
            Rectangle{
                id:batteryRect
                state: "charging"
                states: [
                    State {
                        name:"charging"
                        PropertyChanges {target:battery; animationItem:"qrc:/images/battery.gif" }
                    },
                    State {
                        name:"battery0"
                        PropertyChanges {target:battery; animationItem:"qrc:/images/battery0.gif" }
                    },
                    State {
                        name:"battery1"
                        PropertyChanges {target:battery; animationItem:"qrc:/images/battery1.gif" }
                    },
                    State {
                        name:"battery2"
                        PropertyChanges {target:battery; animationItem:"qrc:/images/battery2.gif" }
                    },
                    State {
                        name:"battery3"
                        PropertyChanges {target:battery; animationItem:"qrc:/images/battery3.gif" }
                    },
                    State {
                        name:"battery4"
                        PropertyChanges {target:battery; animationItem:"qrc:/images/battery4.gif" }
                    }
                ]
            }
        }
    }
}

