/*
    copyright (2015-2018) VersaBox sp. z o.o.
    This file is part of InterfaceApp.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "include/interface/headcameraimageprovider.h"

HeadCameraImageProvider::HeadCameraImageProvider()
    :QQuickImageProvider(QQuickImageProvider::Image)

{
    image=QImage(1920,768,QImage::Format_RGB888);
}

QImage HeadCameraImageProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
    return image;
}

void HeadCameraImageProvider::newImage(QImage image)
{
    this->image=image;
}

